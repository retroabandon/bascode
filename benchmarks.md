Benchmark Results
=================

Some of the programs in this repository have been used as (often rough)
benchmarks comparing various systems.

mandel
------

BASIC ASCII Mandelbrot program by Gordon Henderson (drogon). From
forum.6502.org [Benchmarking][f65 bench] thread.

    */mandel.{bas,asc}          ASCII source
    common/mandel-64-48.txt     64×48 output (BBC micro)

Most versions of this here have the following changes, but check the notes
above for any particular version and the source itself to confirm them.
- Add comment at start noting it's been modified.
- Reduce _w,h_ from 64,48 to 38,21 (to fit onto a 40×24 screen), as that is
  what others in the thread seem mainly to be using.
- Add `165 DEFSNG`, `165 DEFDBL` or similar on systems with multiple
  floating-point precisions.
- Add `285 TIME=0` to avoid negative time results due to rollover.
- Changes to print time taken in seconds, either by using the correct
  divisor for `TIME` or in some cases printing start and end times.

The following table contains timing results. The `size` field gives the
floating point format used as the number of bytes of the significand
(prefixed by `d` for a decimal significand) and exponent (which may include
the significand sign bit and/or other information). A `?` indicates that
the given sizes are unknown or have not been confirmed. `sim/...` indicates
the benchmark was run on a simulator or emulator, and gives the name (and
usually version). Possibly not all benchmarks that have been run on
simulators have so indicated.

     size   secs    descr
      3,1    34.94  IBM PC 5150 (cassette Basic, pce-ibmpc emu)
      4,1    86.93  Apple IIc+ (MAME 0.228 emu)
      3,1    93     ABC 800 (SINGLE default, MAME/MESS emu)
      4,1   101.7   BBC Master 128 (mode 7, real iron)
      4,1   106.75  BBC Master Z80B @6MHz (mode 7, real iron)
      3,1   117     Electronica BK0011M (BK2010, adjusted)
      4,1   123.57  Apple IIgs (MAME 0.239 emu)
      4,1   144.96  BBC Micro B (mode 7, sim/b-em)
      4,1   163.43  Amstrad CPC 6128 (sim/ep128emu)
      4,1   163.73  Amstrad NC200 (sim/MAME)
      7,1   164.66  Electronica BK0011M (real iron)
      6,2   176     Sinclair QL (Qemulator 3.4, it is likely that this result is about 7% faster than from the real iron)
      7,1   205     Electronica BK0010-01 (real iron)
      7,1   205     ABC 800 (DOUBLE, MAME/MESS emu)
      4,1   264.6   Commodore CBM II B128 (sim/VICE)
      4,1   297.57  Commodore 128 (fast sim/VICE)
      4,1   347     MTX512 (real iron)
      4,1   346.37  Apple IIc (MAME 0.239 emu)
      4,1   352.5   Apple IIe (MAME 0.239 emu)
      3,1   371.1   Olivetti Prodest PC128 - a clone of the Thomson MO6 (real iron)
      4,1   384.16  Commodore 64 (sim/VICE)
     d5,1   394.12  Atari 800XL (sim/atari800)
      4,1   393.17  Tandy Color 3 6309 (XRoar 1.0.7 emu)
      4,1   401.82  Dragon-32/64 (XRoar 1.0.7 emu)
      4,1   408.5   TRS-80 Color Computer 1/2 (XRoar 1.0.7 emu)
     d3,1?  410     NEC PC-8201 (DEFSNG default)
     d3,1   411.70  MSX1 (DEFSNG 50Hz, Sony HB-55P EU sim/openMSX v0.15)
     d3,1   412.53  MSX1 (DEFSNG, Sanyo MPC-2 sim/openMSX v0.15)
     d3,1   413.37  MSX1 (DEFSNG, Sanyo MPC-2 Wavy2 JP)
     d3,1   413.45  MSX1 (DEFSNG, Sony HB-55 JP)
     d3,1   435.03  MSX2 (DEFSNG, Sony HB-F1XD sim/openMSX v0.15)
      4,1   454     Tandy Color 3 (XRoar 1.0.7 emu)
      4,1   485.85  Commodore +4 (sim/plus4emu)
     d7,1   520.00  MSX1 (DEFDBL 50Hz, Sony HB-55P EU sim/openMSX v0.15)
     d7,1   521.03  MSX1 (DEFDBL, Sanyo MPC-2 Wavy2 sim/openMSX v0.15)
     d7,1   521.9   MSX1 (DEFDBL, Sanyo MPC-2 Wavy2 JP)
     d7,1   521.98  MSX1 (DEFDBL, Sony HB-55 JP)
     d7,1   549.48  MSX2 (DEFDBL, Sony HB-F1XD w/openMSX v0.15)
     d7,1   554.98  MSX2 (Sanyo MPC-25FD sim/openMSX)
     d3,1   557     TRS-80 Model 100 (DEFSNG)
      3,1   564.92  Corvette PC-8020/30 (b2em emu)
      4,1   620.20  Commodore 128 (sim/VICE)
      4,1   688.80  Commodore CBM II P500 (sim/VICE)
     d7,1   691     TI-99/4A + Editor/Assembler cartridge (Classic99 emu)
      3,1   694.23  TRS-80 Model 3 (DEFSNG default, trs80gp emu)
     d7,1   701     TRS-80 Model 100 (DEFDBL default)
      3,1   773.16  TRS-80 Model 1 (DEFSNG default, trs80gp emu)
     d7,1   774     TI-99/4A + Extended Basic cartridge (Classic99 emu)

[f65 bench]: http://forum.6502.org/viewtopic.php?t=6323
