Simulator/Emulator Usage
========================


openMSX
-------

OpenMSX can load files directly from disk on machines where it emulates a
floppy drive, or by creating a `.cas` cassette image using [MSX CAS
Packager (MCP)][mcp] ([GitHub][mcp-gh]).

    openmsx -diska msx/ -machine Sony_HB-F1XD
    LOAD "foo.asc"

    bin/mcp -a msx/foo.cas msx/foo.asc
    openmsx -machine Sanyo_MPC-2                # Or try Sony_HB-55P
    # F10 to bring up openMSX emulation console
    cassetteplayer insert msx/mandel.cas        # RUN "CAS:" is automatic
    cassetteplayer rewind                       # if you need to reload
    RUN "CAS:"

MCP can also create a cassette image in .WAV format to be played back
into a real MSX machine.

For further information on openMSX cassette emulation commands, see [4.3
Running Tape Software][omsx u4.3] in the _openMSX User's Manual_ and
[cassetteplayer][omsx ccas] in the _openMSX Console Command Reference_.


[mcp]: http://mcp.typeinference.com/
[mpc-gh]: https://github.com/apoloval/mcp
[omsx ccas]: https://openmsx.org/manual/commands.html#cassetteplayer
[omsx u4.3]: https://openmsx.org/manual/user.html#tape
