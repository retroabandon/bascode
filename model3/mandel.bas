10 REM SET MEMORY SIZE TO 65491 !!!
100 REM A BASIC, ASCII MANDELBROT
110 REM
120 REM This implementation copyright (c) 2019, Gordon Henderson
130 REM
140 REM Permission to use/abuse anywhere for any purpose granted, but
150 REM it comes with no warranty whatsoever. Good luck!
160 REM
165 REM Tandy TRS-80 Model 3 variant by litwr, 2021
170 C$ = ".,'~=+:;[/<&?oxOX# " : REM 'Pallet' Lightest to darkest...
180 SO = 1 : REM Set to 0 if your MID$() indexes from 0.
190 MI = LEN(C$)
200 MX = 4
210 LS = -2.0
220 TP = 1.25
230 XS = 2.5
240 YS = -2.5
250 W = 38
260 H = 21
270 SX = XS / W
280 SY = YS / H
285 FOR X=1 TO 42:READ Y:POKE X-45,Y:NEXT
290 Y=65492:X=INT(Y/256):POKE16526,Y-X*256:POKE16527,X:Y=USR(0)
300 FOR Y = 0 TO H
310   CY = Y * SY + TP
320   FOR X = 0 TO W
330     CX = X * SX + LS
340     ZX = 0
350     ZY = 0
360     CC = SO
370     X2 = ZX * ZX
380     Y2 = ZY * ZY
390     IF CC > MI THEN GOTO 460
400     IF (X2 + Y2) > MX THEN GOTO 460
410     T = X2 - Y2 + CX
420     ZY = 2 * ZX * ZY + CY
430     ZX = T
440     CC = CC + 1
450     GOTO 370
460     PRINT MID$(C$, CC - SO, 1);
470   NEXT
480   PRINT
490 NEXT
500 Y=65505:X=INT(Y/256):POKE16526,Y-X*256:Y=USR(0)
510 PRINT (PEEK(-5)+PEEK(-4)*256+PEEK(-3)*65536)/30
520 END
2010 DATA 42,19,64,34,249,255,33,232,255,34,19,64,201,42,249,255
2020 DATA 34,19,64,201,229,245,33,251,255,52,32,6,35,52,32,2
2030 DATA 35,52,241,225,195,0,0,0,0,0

