bascode - BASIC Programs from Various Sources
=============================================

### generic/

- `mandel.bas`: 64×48 ASCII Mandelbrot by Gordon Henderson (drogon).
  `mandel-64x48.txt` is sample output. [[benchmarks]]
- `wumpus.bas`: Hunt the Wumpus, by Gregory Yob (~1975). [[ahl79]]
  [`wumpus-map.png`](wumpus-map.png) shows the map.

### model100/ (TRS-80 Model 100)

- `awari.bas`: Adapted from [[ahl78]].
- `rally.bas`: Driving game; avoid the walls. Has sound. [[bus84]].
- `wumpus.bas`: Conversion of `generic/` version, also good for PC-8201.
  The only BASIC issue was `DEF FN`. Lower-case input is now also accepted.
  Fairly extensive work was done for the smaller screen.
- `mandel.bas`: Print 38×21 ASCII Mandelbrot. [[benchmarks]]

### msx/ (generic MSX1/MSX2 software)

MSX files use a `.asc` extension so that MSX CAS packager will store it as
an ASCII file in a `.cas` image, rather than a tokenized BASIC file. This
is also the convention for ASCII-encoded BASIC files on MSX diskettes,
though MSX-DOS will correctly determine the format no matter what the name.

- `mandel.asc`: Print 38×21 ASCII Mandelbrot. [[benchmarks]]

### pc8201/ (NEC PC-8201)

- `clock.bas`: Real-time clock display with alarm (by queuebert).
- `romaji.bas`: Romaji input for kana v0.1 (by queuebert).


References
----------

- [[ahl78]] David H. Ahl, _BASIC Computer Games (Microcomputer Edition)_.
  Creative Computing, 1978.
- [[ahl79]] David H. Ahl, _More BASIC Computer Games_. Creative Computing,
  1979.
- [[bus84]] David D. Busch, _25 Games for your TRS-80™ Model 100_. Tab
  Books, 1984.



<!-------------------------------------------------------------------->
[ahl79]: https://archive.org/stream/More_BASIC_Computer_Games#page/n0/mode/1up
[benchmarks]: benchmarks.md
