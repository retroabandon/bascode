100 rem a basic, ascii mandelbrot
110 rem
120 rem this implementation copyright (c) 2019, gordon henderson
130 rem
140 rem permission to use/abuse anywhere for any purpose granted, but
150 rem it comes with no warranty whatsoever. good luck!
160 rem
162 rem the bk port by litwr, 2020, 2021, 2023
164 rem mh=3 for the bk0010-01, and mh=4 for bk0011
166 mh=3
170 c$ = ".,'~=+:;[/<&?oxOX# "
180 so! = 1
190 mi! = len(c$)
200 mx! = 4
210 ls! = -2.0
220 tp! = 1.25
230 xs! = 2.5
240 ys! = -2.5
250 w! = 38
260 h! = 21
270 sx! = xs! / w!
280 sy! = ys! / h!
284 poke&hffc6,0
286 poke&hffca,1
290 poke&hffca,114
300 for y! = 0 to h!
310   cy! = y! * sy! + tp!
320   for x! = 0 to w!
330     cx! = x! * sx! + ls!
340     zx! = 0
350     zy! = 0
360     cc! = so!
370     x2! = zx! * zx!
380     y2! = zy! * zy!
390     if cc! > mi! then goto 460
400     if (x2! + y2!) > mx! then goto 460
410     t! = x2! - y2! + cx!
420     zy! = 2 * zx! * zy! + cy!
430     zx! = t!
440     cc! = cc! + 1
450     goto 370
460     print mid$(c$, cc! - so!, 1);
470   next
480   print
482 t1!=peek(&hffc8)
484 t2!=t1!-t0!
486 if t2!<=0 then t3!=t3!-t2! else t3!=t3!+65536-t2!
488 t0!=t1!
490 next
510 print t3!/mh*64/1000*128/1000
