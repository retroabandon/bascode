     org 0ffd0h
start
     ld a,0c3h   ;JP
     ld (4012h),a
     ld hl,intr
     ld (4013h),hl
     ei
     ret

finish
     di
     ld hl,0c9fbh
     ld (4012h),hl
     ;xor a
     ;ld (4014h),a
     ret

intr    push hl
        push af
        ld a,(37e0h)
        ld hl,time
        inc (hl)
        jr nz,icont

        inc hl
        inc (hl)
        jr nz,icont

        inc hl
        inc (hl)
icont   pop af
        pop hl
        ei
        ret

time db 0,0,0

