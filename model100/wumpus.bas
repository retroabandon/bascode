3 SCREEN 0,0:REM Disable fkey label line
5 CLS:PRINT
10 PRINT tab(5);"Wumpus, by Gregory Yob (1975)"
20 PRINT tab(7);"From Ahl, tweaked by cjs."
26 PRINT
30 PRINT "Instructions (Y/N)";
40 INPUT I$
50 IF I$="N" OR I$="n" THEN 60
55 GOSUB 1000
60 REM- SET UP CAVE (DODECAHEDRAL NODE LIST)
70 DIM S(20,3)
80 FOR J=1 TO 20
90 FOR K=1 TO 3
100 READ S(J,K)
110 NEXT K
120 NEXT J
130 DATA 2,5,8,1,3,10,2,4,12,3,5,14,1,4,6
140 DATA 5,7,15,6,8,17,1,7,9,8,10,18,2,9,11
150 DATA 10,12,19,3,11,13,12,14,20,4,13,15,6,14,16
160 DATA 15,17,20,7,16,8,9,17,19,11,18,20,13,16,19
200 REM- LOCATE L ARRAY ITEMS
210 REM- 1-YOU,2-WUMPUS,3&4-PITS,5&6-BATS
220 DIM L(6),M(6)
230 FOR J=1 TO 6
240 L(J)=INT(20*RND(1))+1
260 M(J)=L(J)
270 NEXT J
280 REM- CHECK FOR CROSSOVERS (IE L(1)=L(2) ETC)
290 FOR J=1 TO 6
300 FOR K=J TO 6
310 IF J=K THEN 330
320 IF L(J)=L(K) THEN 240
330 NEXT K
340 NEXT J
350 REM- SET# ARROS
360 A=5
365 L=L(1)
370 REM- RUN THE GAME
375 PRINT:PRINT "***** HUNT THE WUMPUS"
380 REM- HAZARD WARNINGS AND LOCATIONS
390 GOSUB 2000
400 REM- MOVE OR SHOOT
410 GOSUB 2500
420 ON O GOTO 440,480
430 REM- SHOOT
440 GOSUB 3000
450 IF F=0 THEN 390
460 GOTO 500
470 REM- MOVE
480 GOSUB 4000
490 IF F=0 THEN 390
500 IF F > 0 THEN 550
510 REM- LOSE
520 PRINT "Ha ha ha - YOU LOSE!"
530 GOTO 560
540 REM- WIN
550 PRINT "Hee hee-the wumpus'll getcha next time!"
560 FOR J=1 TO 6
570 L(J)=M(J)
580 NEXT J
590 PRINT "Same set-up (Y/N)";
600 INPUT I$
610 IF I$ <> "Y" AND I$ <> "y" THEN 230
620 GOTO 360
1000 REM- INSTRUCTIONS
1005 CLS:PRINT "*** Welcome to 'Hunt the Wumpus'"
1010 PRINT
1020 PRINT "The wumpus lives in a cave of 20 rooms,"
1030 PRINT "each with 3 tunnels leading to other"
1040 PRINT "rooms. Look at a dodecahedron to see"
1050 PRINT "how this works. (If you don't know what"
1060 PRINT "a dodechadron is, ask someone)"
1070 GOSUB 6000:IF PG<0 THEN 1000
1100 CLS:PRINT "*** Hazards"
1110 PRINT
1120 PRINT "Hazards are two bottomless pits,"
1130 PRINT "which kill you if you fall into them,"
1140 PRINT "and two rooms with superbats, which"
1150 PRINT "grab you and take you to a random"
1160 PRINT "room (which might be troublesome)."
1170 GOSUB 6000:IF PG<0 THEN 1000
1200 CLS:PRINT "*** The Wumpus"
1210 PRINT
1220 PRINT "The wumpus is not bothered by the"
1230 PRINT "hazards (he has sucker feet and is too"
1240 PRINT "big for a bat to lift). Usually he is"
1250 PRINT "asleep. Entering his room or shooting"
1260 PRINT "an arrow will wake him up."
1270 GOSUB 6000:IF PG<0 THEN 1100
1300 CLS:PRINT "*** The Wumpus (cont.)"
1310 PRINT
1320 PRINT "If the wumpus wakes, he moves (p=.75)"
1330 PRINT "one room or stays still (p=.25). After"
1340 PRINT "that, if he is where you Are, he eats"
1350 PRINT "you up (and you lose!)."
1360 PRINT
1370 GOSUB 6000:IF PG<0 THEN 1200
1400 CLS:PRINT "*** Gameplay"
1410 PRINT
1420 PRINT "Each turn you may move or shoot a"
1430 PRINT "crooked arrow. Moving goes through a"
1440 PRINT "tunnel to an adjacent room. Shooting an"
1450 PRINT "arrow uses one of your five arrows; you"
1460 PRINT "die when you run out of arrows."
1470 GOSUB 6000:IF PG<0 THEN 1300
1500 CLS:PRINT "*** Aiming Arrows"
1510 PRINT
1520 PRINT "Each arrow can go from 1 to 5 rooms."
1530 PRINT "You aim by telling the computer the"
1540 PRINT "rooms through which the arrow should"
1550 PRINT "pass. If there's no tunnel to that"
1560 PRINT "room, the arrow enters a random room."
1570 GOSUB 6000:IF PG<0 THEN 1400
1600 CLS:PRINT "*** Arrow Results"
1610 PRINT
1620 PRINT "If the arrow hits the wumpus, you win."
1630 PRINT "If the arrow hits you, you die."
1640 PRINT "Remember that the wumpus may move"
1650 PRINT "after you shoot an arrow."
1660 PRINT
1670 GOSUB 6000:IF PG<0 THEN 1500
1700 CLS:PRINT "*** Warnings"
1710 PRINT
1720 PRINT "When you are one room away from the"
1730 PRINT "wumpus or a hazard, the computer says:"
1740 PRINT "* Wumpus: 'I smell a wumpus.'"
1750 PRINT "* Bat:    'I hear bats nearby.'"
1760 PRINT "* Pit:    'I feel a draft.'"
1770 GOSUB 6000:IF PG<0 THEN 1600
1800 CLS
1810 RETURN
2000 REM- PRINT  LOCATION & HAZARD WARNINGS
2020 FOR J= 2 TO 6
2030 FOR K=1 TO 3
2040 IF S(L(1),K)<>L(J) THEN 2110
2050 ON J-1 GOTO 2060,2080,2080,2100,2100
2060 PRINT "I smell a wumpus."
2070 GOTO 2110
2080 PRINT "I feel a draft."
2090 GOTO 2110
2100 PRINT "I hear bats nearby."
2110 NEXT K
2120 NEXT J
2130 PRINT "Room ";L(1);
2140 PRINT "with tunnels to ";S(L,1);S(L,2);S(L,3)
2160 RETURN
2500 REM- CHOOSE OPTION
2510 PRINT "    Shoot/Move (S/M)";
2520 INPUT I$
2530 IF I$ <> "S" and I$ <> "s" THEN 2560
2540 O=1
2550 RETURN
2560 IF I$ <> "M" and I$ <> "m" THEN 2510
2570 O=2
2580 RETURN
3000 REM- ARROW ROUTINE
3010 F=0
3020 REM- PATH OF ARROW
3030 L=L(1)
3040 PRINT "    No. of rooms (1-5)";
3050 INPUT J9
3060 IF J9<1 OR J9>5 THEN 3040
3070 FOR K=1 TO J9
3080 PRINT "    Room #";
3090 INPUT P(K)
3095 IF K <= 2 THEN 3115
3100 IF P(K) <> P(K-2) THEN 3115
3105 PRINT "Arrows aren't that crooked - try another room."
3110 GOTO 3080
3115 NEXT K
3120 REM- SHOOT ARROW
3140 FOR K=1 TO J9
3150 FOR K1=1 TO 3
3160 IF S(L,K1)=P(K) THEN 3295
3170 NEXT K1
3180 REM- NO TUNNEL FOR ARROW
3190 L=S(L,INT(3*RND(1))+1)
3200 GOTO 3300
3210 NEXT K
3220 PRINT "    Missed."
3225 L=L(1)
3230 REM- MOVE WUMPUS
3240 GOSUB 3370
3250 REM- AMMO CHECK
3255 A=A-1
3260 IF A>0 THEN 3280
3270 F=-1
3280 RETURN
3290 REM- SEE IF ARROW IS AT L(1) OR L(2)
3295 L=P(K)
3300 IF L <> L(2) THEN 3340
3310 PRINT "Aha! You got the wumpus!"
3320 F=1
3330 RETURN
3340 IF L <> L(1) THEN 3210
3350 PRINT "Ouch! Arrow got you!"
3360 GOTO 3270
3370 REM- MOVE WUMPUS ROUTINE
3380 K=INT(4*RND(1))+1
3390 IF K=4 THEN 3410
3400 L(2)=S(L(2),K)
3410 IF L(2) <> L THEN 3440
3420 PRINT "Tsk tsk tsk - wumpus got you!"
3430 F=-1
3440 RETURN
4000 REM- MOVE ROUTINE
4010 F=0
4020 PRINT "    Move to what room";
4030 INPUT L
4040 IF L<1 OR L>20 THEN 4020
4050 FOR K=1 TO 3
4060 REM- CHECK IF LEGAL MOVE
4070 IF S(L(1),K)=L THEN 4130
4080 NEXT K
4090 IF L=L(1) THEN 4130
4100 PRINT "    No path! ";
4110 GOTO 4020
4120 REM- CHECK FOR HAZARDS
4130 L(1)=L
4140 REM- WUMPUS
4150 IF L <> L(2) THEN 4220
4160 PRINT "    ... Oops! Bumped a wumpus!"
4170 REM- MOVE WUMPUS
4180 GOSUB 3380
4190 IF F=0 THEN 4220
4200 RETURN
4210 REM- PIT
4220 IF L <> L(3) AND L <> L(4) THEN 4270
4230 PRINT "Yyyiiiieeee . . . fell in pit"
4240 F=-1
4250 RETURN
4260 REM- BATS
4270 IF L <> L(5) AND L <> L(6) THEN 4310
4280 PRINT "    Bat snach! Elsewhereville for you!"
4290 L=INT(20*RND(1))+1
4300 GOTO 4130
4310 RETURN
5000 REM END
6000 REM Page forward/back for instructions.
6010 PRINT "---space: next page; 'b': prev page---";
6020 PG$=INKEY$:IF PG$="" GOTO 6020
6030 IF PG$=" " THEN PG=0:RETURN
6040 IF PG$="B" OR PG$="b" THEN PG=-1:RETURN
6050 GOTO 6020
7000 END
